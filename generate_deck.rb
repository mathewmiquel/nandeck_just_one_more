words = Array.new
File.open('words.txt').each { |line| words << line.strip.upcase }

cards_count = words.count / 7

all = '"1-' + cards_count.to_s + '"'

puts 'CARDSIZE=6.9,9.4
DPI=350

RECTANGLE=' + all + ',1.2,0.55,5.0,1.1,#FF0000,#FFA6A6,0.05
RECTANGLE=' + all + ',1.2,1.75,5.0,1.1,#FF7F00,#FFD3AB,0.05
RECTANGLE=' + all + ',1.2,2.95,5.0,1.1,#FFFF00,#FFFFAB,0.05
RECTANGLE=' + all + ',1.2,4.15,5.0,1.1,#00AD43,#9BE8BA,0.05
RECTANGLE=' + all + ',1.2,5.35,5.0,1.1,#1F75FE,#A3C7FE,0.05
RECTANGLE=' + all + ',1.2,6.55,5.0,1.1,#091F92,#ADBCFF,0.05
RECTANGLE=' + all + ',1.2,7.75,5.0,1.1,#7F00FF,#D5B0FF,0.05

FONT="Times New Roman", 18, "T", "#FF0000"
TEXT=' + all + ', "1.", 0.6, 0.6, 5.7, 1.0, "left", "wwcenter"
FONT="Times New Roman", 18, "T", "#FF7F00"
TEXT=' + all + ', "2.", 0.6, 1.8, 5.7, 1.0, "left", "wwcenter"
FONT="Times New Roman", 18, "T", "#FFFF00"
TEXT=' + all + ', "3.", 0.6, 3.0, 5.7, 1.0, "left", "wwcenter"
FONT="Times New Roman", 18, "T", "#00AD43"
TEXT=' + all + ', "4.", 0.6, 4.2, 5.7, 1.0, "left", "wwcenter"
FONT="Times New Roman", 18, "T", "#1F75FE"
TEXT=' + all + ', "5.", 0.6, 5.4, 5.7, 1.0, "left", "wwcenter"
FONT="Times New Roman", 18, "T", "#091F92"
TEXT=' + all + ', "6.", 0.6, 6.6, 5.7, 1.0, "left", "wwcenter"
FONT="Times New Roman", 18, "T", "#7F00FF"
TEXT=' + all + ', "7.", 0.6, 7.8, 5.7, 1.0, "left", "wwcenter"

FONT="Times New Roman", 18, "T", "#000000"
'

for i in 1..cards_count
	start_index = (i-1) * 7
	puts 'TEXT="'+i.to_s+'", "'+ words[start_index+0] +'", 1.2, 0.6, 5, 1.0, "center", "wwcenter"'
	puts 'TEXT="'+i.to_s+'", "'+ words[start_index+1] +'", 1.2, 1.8, 5, 1.0, "center", "wwcenter"'
	puts 'TEXT="'+i.to_s+'", "'+ words[start_index+2] +'", 1.2, 3.0, 5, 1.0, "center", "wwcenter"'
	puts 'TEXT="'+i.to_s+'", "'+ words[start_index+3] +'", 1.2, 4.2, 5, 1.0, "center", "wwcenter"'
	puts 'TEXT="'+i.to_s+'", "'+ words[start_index+4] +'", 1.2, 5.4, 5, 1.0, "center", "wwcenter"'
	puts 'TEXT="'+i.to_s+'", "'+ words[start_index+5] +'", 1.2, 6.6, 5, 1.0, "center", "wwcenter"'
	puts 'TEXT="'+i.to_s+'", "'+ words[start_index+6] +'", 1.2, 7.8, 5, 1.0, "center", "wwcenter"'
end
