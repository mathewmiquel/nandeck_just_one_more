words = Array.new
File.open('guy_2021_FINAL.csv').each { |line| words << line.strip.upcase }

cards_count = words.count / 7

all = '"1-' + cards_count.to_s + '"'

puts 'CARDSIZE=6.9,9.4
DPI=350

RECTANGLE=' + all + ',1.2,0.55,5.0,1.1,#00CED1,#00CED1,0.05
RECTANGLE=' + all + ',1.2,1.75,5.0,1.1,#FEDF00,#FEDF00,0.05
RECTANGLE=' + all + ',1.2,2.95,5.0,1.1,#9C51B6,#9C51B6,0.05
RECTANGLE=' + all + ',1.2,4.15,5.0,1.1,#FF5800,#FF5800,0.05
RECTANGLE=' + all + ',1.2,5.35,5.0,1.1,#32CD32,#32CD32,0.05
RECTANGLE=' + all + ',1.2,6.55,5.0,1.1,#000000,#000000,0.05
RECTANGLE=' + all + ',1.2,7.75,5.0,1.1,#ED2939,#ED2939,0.05



FONT="Times New Roman", 18, "T", "#87CEEB"
TEXT=' + all + ', "1.", 0.6, 0.6, 5.7, 1.0, "left", "wwcenter"
FONT="Times New Roman", 18, "T", "#FEDF00"
TEXT=' + all + ', "2.", 0.6, 1.8, 5.7, 1.0, "left", "wwcenter"
FONT="Times New Roman", 18, "T", "#9C51B6"
TEXT=' + all + ', "3.", 0.6, 3.0, 5.7, 1.0, "left", "wwcenter"
FONT="Times New Roman", 18, "T", "#FF5800"
TEXT=' + all + ', "4.", 0.6, 4.2, 5.7, 1.0, "left", "wwcenter"
FONT="Times New Roman", 18, "T", "#32CD32"
TEXT=' + all + ', "5.", 0.6, 5.4, 5.7, 1.0, "left", "wwcenter"
FONT="Times New Roman", 18, "T", "#000000"
TEXT=' + all + ', "6.", 0.6, 6.6, 5.7, 1.0, "left", "wwcenter"
FONT="Times New Roman", 18, "T", "#ED2939"
TEXT=' + all + ', "7.", 0.6, 7.8, 5.7, 1.0, "left", "wwcenter"

'

for i in 1..cards_count
	start_index = (i-1) * 7
	puts 'FONT="Times New Roman", ' + (words[start_index+0].length >= 12 ? '14' : '18') + ', "T", "#000000"'
	puts 'TEXT="'+i.to_s+'", "'+ words[start_index+0] +'", 1.2, 0.6, 5, 1.0, "center", "wwcenter"'
	puts 'FONT="Times New Roman", ' + (words[start_index+1].length >= 12 ? '14' : '18') + ', "T", "#000000"'
	puts 'TEXT="'+i.to_s+'", "'+ words[start_index+1] +'", 1.2, 1.8, 5, 1.0, "center", "wwcenter"'
	puts 'FONT="Times New Roman", ' + (words[start_index+2].length >= 12 ? '14' : '18') + ', "T", "#FFFFFF"'
	puts 'TEXT="'+i.to_s+'", "'+ words[start_index+2] +'", 1.2, 3.0, 5, 1.0, "center", "wwcenter"'
	puts 'FONT="Times New Roman", ' + (words[start_index+3].length >= 12 ? '14' : '18') + ', "T", "#000000"'
	puts 'TEXT="'+i.to_s+'", "'+ words[start_index+3] +'", 1.2, 4.2, 5, 1.0, "center", "wwcenter"'
	puts 'FONT="Times New Roman", ' + (words[start_index+4].length >= 12 ? '14' : '18') + ', "T", "#000000"'
	puts 'TEXT="'+i.to_s+'", "'+ words[start_index+4] +'", 1.2, 5.4, 5, 1.0, "center", "wwcenter"'
	puts 'FONT="Times New Roman", ' + (words[start_index+5].length >= 12 ? '14' : '18') + ', "T", "#FFFFFF"'
	puts 'TEXT="'+i.to_s+'", "'+ words[start_index+5] +'", 1.2, 6.6, 5, 1.0, "center", "wwcenter"'
	puts 'FONT="Times New Roman", ' + (words[start_index+6].length >= 12 ? '14' : '18') + ', "T", "#000000"'
	puts 'TEXT="'+i.to_s+'", "'+ words[start_index+6] +'", 1.2, 7.8, 5, 1.0, "center", "wwcenter"'
end
